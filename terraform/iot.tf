resource "aws_iot_certificate" "cert" {
  active = true
}

resource "aws_iot_policy" "pubsub" {
  name = "tp_policy"
  policy = file("files/iot_policy.json")
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}

resource "aws_iot_thing" "temp_sensor" {
  name = "temp_sensor"
}

resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}

data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

resource "aws_iot_topic_rule" "iot_topic_rule" {
  name        = "temperature_rule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    database_name = aws_timestreamwrite_database.db_iot_stream.database_name
    table_name    = aws_timestreamwrite_table.table_iot_stream.table_name
    role_arn      = aws_iam_role.iot_role.arn

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}

# aws_iot_topic_rule rule for sending valid data to Timestream


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
