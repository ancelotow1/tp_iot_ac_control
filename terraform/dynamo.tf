resource "aws_dynamodb_table" "temperature" {
  name         = "Temperature"
  billing_mode = "PROVISIONED"
  hash_key     = "id"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "id"
    type = "S"
  }
}

resource "aws_iot_topic_rule" "temperature_rule" {
  name        = "TemperatureRule"
  description = "Rule to get temperature"
  enabled     = true
  sql         = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature.name
    }
  }
}

resource "aws_iam_role" "iot_role" {
  name               = "iot_role"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "iot.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy" "test_policy" {
  name = "iot_role_policy"
  role = aws_iam_role.iot_role.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "dynamodb:PutItem"
        ],
        "Resource" : [
          aws_dynamodb_table.temperature.arn
        ]
      }
    ]
  })
}