resource "aws_timestreamwrite_database" "db_iot_stream" {
  database_name = "iot"
}

resource "aws_timestreamwrite_table" "table_iot_stream" {
  database_name = aws_timestreamwrite_database.db_iot_stream.database_name
  table_name    = "temperaturesensor"
}
